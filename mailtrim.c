/* mailtrim - trim mailbox by removing outdated messages in-place
   Copyright (C) 2022 Sergey Poznyakoff

   Mailtrim is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Mailtrim is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with mailtrim. If not, see <http://www.gnu.org/licenses/>. */

#define _DEFAULT_SOURCE
#define _XOPEN_SOURCE
#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>

/*
 * A modified version of Marc Crispin VALID macro.
 *
 * This function handles all existing flavors of the From_ line, most
 * of which are antiquated and fallen out of use:
 *
 *              From user Wed Dec  2 05:53 1992
 * BSD          From user Wed Dec  2 05:53:22 1992
 * SysV         From user Wed Dec  2 05:53 PST 1992
 * rn           From user Wed Dec  2 05:53:22 PST 1992
 *              From user Wed Dec  2 05:53 -0700 1992
 *              From user Wed Dec  2 05:53:22 -0700 1992
 *              From user Wed Dec  2 05:53 1992 PST
 *              From user Wed Dec  2 05:53:22 1992 PST
 *              From user Wed Dec  2 05:53 1992 -0700
 * Solaris      From user Wed Dec  2 05:53:22 1992 -0700
 *
 * (plus all of them followed by " remote from xxx").  Moreover, the
 * user part is allowed to have whitespaces in it (although mailutils
 * email address functions won't tolerate it).
 * 
 * Input: S - line read from the mailbox (with \n terminator).
 * Output: ZP - points to the space character preceding the time zone
 * information in S.  If there is no TZ, points to the terminating \n.
 *
 * Return value: If S is a valid From_ line, a pointer to the time
 * information in S.  Otherwise, NULL.
 */
static inline char *
parse_from_line (char const *s, char **zp)
{
  int ti = 0;
  int zn;
  // Validate if the string begins with 'From '
  if ((*s == 'F') && (s[1] == 'r') && (s[2] == 'o') && (s[3] == 'm') && 
      (s[4] == ' '))
    {
      char *x = strchr (s, '\n');
      if (x)
	{
	  if (x - s >= 41)
	    {
	      static char suf[] = " remote from ";
#             define suflen (sizeof(suf)-1)

	      for (zn = -1; x + zn > s && x[zn] != ' '; (zn)--);
	      if (memcmp (x + zn - suflen + 1, suf, suflen) == 0)
		x += zn - suflen + 1;
	    }
	  if (x - s >= 27)
	    {
	      if (x[-5] == ' ')
		{
		  if (x[-8] == ':')
		    {
		      zn = 0;
		      ti = -5;
		    }
		  else if (x[-9] == ' ')
		    ti = zn = -9;
		  else if ((x[-11] == ' ')
			   && ((x[-10] == '+') || (x[-10] == '-')))	      
		    ti = zn = -11;
		}							      
	      else if (x[-4] == ' ')                          
		{                                                 
		  if (x[-9] == ' ')
		    {
		      zn = -4;
		      ti = -9;
		    }
		}
	      else if (x[-6] == ' ')                                        
		{                                                           
		  if ((x[-11] == ' ') && ((x[-5] == '+') || (x[-5] == '-')))
		    {
		      zn = -6;
		      ti = -11;
		    }
		}
	      if (ti && !((x[ti - 3] == ':') &&
			  (x[ti -= ((x[ti - 6] == ':') ? 9 : 6)] == ' ') &&
			  (x[ti - 3] == ' ') && (x[ti - 7] == ' ') &&
			  (x[ti - 11] == ' ')))
		ti = 0;
	    }
	}
      if (ti)
	{
	  *zp = x + zn;
	  return (char*) x + ti;
	}
    }
  return NULL;
}

static char *progname;
static char *filename;
static int verbose;
static int dry_run;

static void
xperror (char const *mesg)
{
  int ec = errno;
  fprintf (stderr, "%s: ", progname);
  errno = ec;
  perror (mesg);
}

static unsigned
find_pos (FILE *fp, time_t stop_time)
{
  char *buf = NULL;
  size_t size = 0;
  ssize_t n;
  unsigned i = 0;
  
  while ((n = getline (&buf, &size, fp)) > 0)
    {
      char *zn, *ti;

      if ((ti = parse_from_line (buf, &zn)) != NULL)
	{
	  struct tm tm;
	  char *p;
	  
	  p = strptime (ti - 10, "%a %b %e %H:%M:%S %Y", &tm);
	  if (p)
	    {
	      if (timegm (&tm) > stop_time)
		{
		  fseek (fp, - n, SEEK_CUR);
		  return i + 1;
		}
	    }
	  else
	    fprintf (stderr, "%s: %s:%d: can't parse\n", progname, filename, i);
	  i++;
	}
      else if (i == 0)
	{
	  fprintf (stderr, "%s: %s: doesn't look like a valid mbox\n",
		   progname, filename);
	  exit (2);
	}
    }
  return 0;
}

static int
time_arg (char const *arg, time_t *tp)
{
  struct tm tm;
  char *p;

  memset (&tm, 0, sizeof (tm));
  if ((p = strptime (arg, "%Y-%m-%dT%H:%M:%S", &tm)) == NULL &&
      (p = strptime (arg, "%Y-%m-%dT%H:%M", &tm)) == NULL &&
      (p = strptime (arg, "%Y-%m-%dT%H", &tm)) == NULL &&
      (p = strptime (arg, "%Y-%m-%d", &tm)) == NULL)
    {
      long n;
      errno = 0;
      n = strtol (arg, &p, 10);
      if (errno || *p)
	return -1;
      *tp = time (NULL) + n;
      return 0;
    }
  if (*p)
    return -1;
  *tp = timegm (&tm);
  return 0;
}     

static void
shiftup (FILE *fp)
{
  size_t size;
  char *buf;
  off_t start;
  size_t n;
  
  start = ftello (fp);
  size = sysconf (_SC_PAGESIZE);
  if ((buf = malloc (size)) == NULL)
    {
      fprintf (stderr, "%s: not enough memory\n", progname);
      exit (2);
    }

  while ((n = fread (buf, 1, size, fp)) > 0)
    {
      if (fseeko (fp, - (start + (off_t)n), SEEK_CUR))
	{
	  xperror ("fseek");
	  exit (3);
	}

      if (fwrite (buf, 1, n, fp) != n)
	{
	  if (ferror (fp))
	    {
	      xperror ("write");
	    }
	  else
	    {
	      fprintf (stderr, "%s: %s: write error (out of disk space?)\n",
		       progname, filename);
	    }
	  exit (3);
	}
      
      if (fseeko (fp, start, SEEK_CUR))
	{
	  xperror ("fseek");
	  exit (3);
	}
    }
  free (buf);
  
  if (ferror (fp))
    {
      xperror ("fread");
      exit (3);
    }
  
  fflush (fp);
  if (fseeko (fp, - start, SEEK_END))
    {
      xperror ("fseek");
      exit (3);
    }
  ftruncate (fileno (fp), ftello (fp));
}

void
usage (int ec)
{
  FILE *fp = ec ? stderr : stdout;
  fprintf (fp, "usage: %s -nVvh [-a DATE] [-b DATE] FILE\n", progname);
  fprintf (fp, "Trims mailbox FILE in-place by removing messages dated before\n");
  fprintf (fp, "and after given dates\n");
  fprintf (fp, "\n");
  fprintf (fp, "OPTIONS:\n");
  fprintf (fp, "   -a DATE  retain messages at or after DATE\n");
  fprintf (fp, "   -b DATE  retain messages before or at DATE\n");
  fprintf (fp, "   -n      dry-run; print what would have been done, do nothing\n");
  fprintf (fp, "           (implies -v)\n");
  fprintf (fp, "   -v      verbose mode\n");
  fprintf (fp, "\n");
  fprintf (fp, "   -V      print version number and exit\n");
  fprintf (fp, "   -h      print this help text\n");
  fprintf (fp, "\n");
  fprintf (fp, "Allowed formats for DATE:\n");
  fprintf (fp, "  1970-01-01T00:00:00\n");
  fprintf (fp, "  1970-01-01T00:00\n");
  fprintf (fp, "  1970-01-01T00\n");
  fprintf (fp, "  1970-01-01\n");
  fprintf (fp, "  3600   # one hour from now\n");
  fprintf (fp, "  -3600  # one hour before\n");
  fprintf (fp, "\n");
  fprintf (fp, "Note that the tool RETAINS messages between -a DATE and -b DATE.\n");
  fprintf (fp, "\n");
  fprintf (fp, "Exit codes: 0 - success, 1 - usage error, 2 - non-fatal error,\n");
  fprintf (fp, "3 - fatal error (mailbox botched).\n");
  fprintf (fp, "\n");  
  fprintf (fp, "Report bugs to <%s>.\n", PACKAGE_BUGREPORT);
  exit (ec);
}

void
version (void)
{
  printf("%s (%s) %s\n", progname, PACKAGE_NAME, PACKAGE_VERSION);
  printf("Copyright (C) %d Sergey Poznyakoff\n", 2022);
  printf("\
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\nThis is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n\
");
}


int
main (int argc, char **argv)
{
  int c;
  FILE *fp;
  struct flock lk;
  time_t time_before = 0, time_after = 0;
  unsigned n;

  progname = argv[0];
  while ((c = getopt (argc, argv, "a:b:hnVv")) != EOF)
    {
      switch (c)
	{
	case 'a':
	  if (time_arg (optarg, &time_after) == -1)
	    {
	      fprintf (stderr, "%s: bad timespec: %s\n", progname, optarg);
	      usage (1);
	    }
	  break;

	case 'b':
	  if (time_arg (optarg, &time_before) == -1)
	    {
	      fprintf (stderr, "%s: bad timespec: %s\n", progname, optarg);
	      usage (1);
	    }
	  break;
	  
	case 'h':
	  usage (0);

	case 'n':
	  dry_run++;
	  /* fall through */
	case 'v':
	  verbose++;
	  break;

	case 'V':
	  version ();
	  exit (0);
	  
	default:
	  exit (1);
	}
    }

  argc -= optind;
  argv += optind;

  if (argc != 1)
    {
      fprintf (stderr, "%s: exactly one argument expected\n", progname);
      usage (1);
    }
  
  if (time_after > 0 && time_before > 0 && time_before <= time_after)
    {
      fprintf (stderr,
	       "%s: time before (-b) must be later than time after (-a)\n",
	       progname);
      exit (1);
    }
  
  filename = argv[0];

  fp = fopen (filename, "r+");
  if (fp == NULL)
    {
      xperror (filename);
      exit (2);
    }
  lk.l_type = F_WRLCK;
  lk.l_whence = SEEK_SET;
  lk.l_start = 0;
  lk.l_len = 0; /* lock entire file */
  if (fcntl (fileno (fp), F_SETLKW, &lk)) 
    {
      fprintf (stderr,
	       "%s: can't lock file: %s\n",
	       progname, strerror (errno));
      exit (1);
    }

  if (time_after > 0)
    {
      if ((n = find_pos (fp, time_after)) > 0)
	{
	  if (verbose)
	    {
	      if (n == 1)
		printf ("%s: removing message %d\n", progname, n);
	      else
		printf ("%s: removing messages 1-%d\n", progname, n-1);
	    }
	  if (!dry_run)
	    shiftup (fp);
	}
      else
	{
	  if (verbose)
	    printf ("%s: removing all messages\n", progname);
	  if (!dry_run)
	    {
	      fflush (fp);
	      ftruncate (fileno (fp), 0);
	    }
	}
    }
  if (time_before > 0)
    {
      fseek (fp, 0, SEEK_SET);
      if ((n = find_pos (fp, time_before)) > 0)
	{
	  if (verbose)
	    printf ("%s: removing messages starting from %d\n", progname, n);
	  if (!dry_run)
	    {
	      off_t off = ftello (fp);
	      fflush (fp);
	      ftruncate (fileno (fp), off);
	    }
	}
    }

  lk.l_type = F_UNLCK;
  lk.l_whence = SEEK_SET;
  lk.l_start = 0;
  lk.l_len = 0; /* lock entire file */
  if (fcntl (fileno (fp), F_SETLKW, &lk)) 
    {
      fprintf (stderr,
	       "%s: can't lock file: %s\n",
	       progname, strerror (errno));
      exit (1);
    }
  
  fclose (fp);
  return 0;
}

    
  
