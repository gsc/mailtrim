# mailtrim - expand environment variables in input files
# Copyright (C) 2022 Sergey Poznyakoff
#
# Mailtrim is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# Mailtrim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with mailtrim. If not, see <http://www.gnu.org/licenses/>.

CFLAGS  = -O2
PREFIX  = /usr/local
MANDIR  = $(PREFIX)/share/man
BINDIR  = $(PREFIX)/bin
MAN1DIR = $(MANDIR)/man1

# Install program.  Use cp(1) if not available.
INSTALL = install
# Program to make a directory hierarchy.
MKHIER  = install -d

# #############################
# Building the program.
# #############################

PACKAGE_NAME    = mailtrim
PACKAGE_VERSION = 1.0
PACKAGE_TARNAME = $(PACKAGE_NAME)-$(PACKAGE_VERSION)
PACKAGE_BUGREPORT = gray@gnu.org
DFLAGS = -DPACKAGE_NAME=\"$(PACKAGE_NAME)\" \
 -DPACKAGE_VERSION=\"$(PACKAGE_VERSION)\" \
 -DPACKAGE_BUGREPORT=\"$(PACKAGE_BUGREPORT)\"

mailtrim: mailtrim.c Makefile
	$(CC) $(CFLAGS) $(DFLAGS) -omailtrim mailtrim.c
clean:
	rm -f mailtrim

# ###############################
# Install
# ###############################

install: install-bin install-man

install-bin: mailtrim
	$(MKHIER) $(DESTDIR)$(BINDIR)
	$(INSTALL) mailtrim $(DESTDIR)$(BINDIR)

install-man: mailtrim.1
	$(MKHIER) $(DESTDIR)$(MAN1DIR)
	$(INSTALL) mailtrim.1 $(DESTDIR)$(MAN1DIR)

# ###############################
# Distribution
# ###############################
DISTDIR = $(PACKAGE_TARNAME)
DISTFILES = Makefile mailtrim.c mailtrim.1 README

distdir: $(DISTFILES)
	rm -rf $(DISTDIR)
	mkdir $(DISTDIR)
	cp $(DISTFILES) $(DISTDIR)

dist: distdir
	tar zcf $(PACKAGE_TARNAME).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)

distcheck: dist
